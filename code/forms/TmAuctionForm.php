<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 11/05/17
 * Time: 7:21 AM
 */
class TmAuctionForm extends Form {

	public function __construct(Controller $controller, $name) {
		$id = $controller->getRequest()->getVar('id');
		if ($id) {
			$obj = TmAuction::get()->filter('ID', $id)->first();
			$product = $obj->Product();
			$pid = $product->ID;
		} else {
			$obj = TmAuction::create();
			$pid = $controller->getRequest()->getVar('pid');
			$product = null;
			if ($pid) {
				$product = Product::get()->filter('ID', $pid)->first();
			}
		}

		$fields = $obj->scaffoldFormFields();
		$fields->add(HiddenField::create('ID', 'ID', $id));
		$fields->add(HiddenField::create('PID', 'PID', $pid));

		$fields->replace($fields->fieldByName('CategoryID'), SilverStripeChosenDropdownField::create('CategoryID', 'Category', TmCategory::LeafCategories()));

		$fields->removeByName('EndDateTime');
		$fields->removeByName('IsClassified');
		$fields->removeByName('ExternalReferenceId');
		$fields->removeByName('ForeignID');
		$fields->removeByName('ProductID');

		$shippingOptions = $obj->ShippingOptions()->count() ? $obj->ShippingOptions()->map('ID', 'ID')->toArray() : array();
		$shippingOption = count($shippingOptions) ? reset($shippingOptions) : null;
		$fields->add(OptionsetField::create('ShippingOptions', 'Shipping Options',
			 	TmShippingOption::get()->map('ID', 'Method')->toArray(),
		  		$shippingOption
			)
		);
		$paymentMethods = $obj->PaymentMethods()->count() ? $obj->PaymentMethods()->map('ID', 'ID')->toArray() : array();
		$fields->add(CheckboxSetField::create('PaymentMethods', 'Payment Methods',
		  		TmPaymentMethod::get()->map('ID', 'Title')->toArray(),
		  		$paymentMethods
			)
		);

		$fields->fieldByName('Quantity')->setTitle('Quantity (Setting Quantity makes this a buy now only auction.)');

		$actions = FieldList::create(
		  FormAction::create('createAuction', 'Create')
		);
		$validator = RequiredFields::create(
		  'Name', 'CategoryID', 'StartPrice', 'BuyNowPrice', 'Duration', 'Description', 'ShippingOptions'
		);

		parent::__construct($controller, $name, $fields, $actions, $validator);

		if ($product && !$obj->ID) {
			$this->fillForm($product, $fields);
		} elseif($obj->ID) {
			$this->loadDataFrom($obj);
			$this->Fields()->dataFieldByName('ShippingOptions')->setValue($shippingOption);
		}
	}

	public function createAuction($data, TmAuctionForm $form) {
		$id = $data['ID'];
		if ($id) {
			$obj = TmAuction::get()->filter('ID', $id)->first();
		} else {
			$obj = TmAuction::create();
		}

		$obj->ProductID = $data['PID'];
		$form->saveInto($obj);
		$obj->write();

		foreach ($data['PaymentMethods'] as $methodID) {
			$obj->PaymentMethods()->add(TmPaymentMethod::get()->filter('ID', $methodID)->first());
		}
		$obj->ShippingOptions()->add(TmShippingOption::get()->filter('ID', $data['ShippingOptions'])->first());

		try {
			$auction = TmApi::create()->Listing();
			$result = $auction->createListing($obj);

			if (is_array($result)) {
				if (isset($result['Success']) && $result['Success'] == false) {
					$this->sessionMessage($result['Description'], 'bad');
					return $this->controller->redirect(Controller::join_links(TmCreateAuctionPage::get()->first()->Link(), '?id=' . $obj->ID));
				} elseif(isset($result['Success']) && $result['Success'] == true) {
					$obj->ForeignID = $result['ListingId'];
					$obj->write();
					return $this->controller->redirect(TmListAuctionsPage::get()->first()->Link());
				}
			}
		} catch (Exception $E) {
			$this->setMessage('Problem creating Auction: ' . $E->getMessage(), 'bad');
			$obj->delete();
			return $this->controller->redirectBack();
		}
	}

	protected function fillForm($product, $fields) {
		$fields->fieldByName('Title')->setValue($product->Name);
		$fields->fieldByName('Description')->setValue($product->Description);
		$fields->fieldByName('StartPrice')->setValue($product->Price);
		$fields->fieldByName('ReservePrice')->setValue($product->Price);
		$fields->fieldByName('BuyNowPrice')->setValue($product->Price);
		$fields->fieldByName('IsBrandNew')->setValue(true);
		//$fields->fieldByName('Quantity')->setValue(1);
		$fields->fieldByName('SKU')->setValue($product->SKU);
	}
}