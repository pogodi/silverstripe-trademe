<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 10/05/17
 * Time: 6:32 PM
 */
class TmCreateAuctionPage extends Page {

}

class TmCreateAuctionPage_Controller extends Page_Controller {

	private static $allowed_actions = array(
	  'TmAuctionForm'
	);

	public function TmAuctionForm() {
		return TmAuctionForm::create($this, 'TmAuctionForm');
	}
}