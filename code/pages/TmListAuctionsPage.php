<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 10/05/17
 * Time: 6:23 PM
 */
class TmListAuctionsPage extends Page {

	public function Auctions() {
		return TmAuction::running();
	}
}

class TmListAuctionsPage_Controller extends Page_Controller {

	private static $allowed_actions = array(
	  	'deleteAuction'
	);

	public function init() {
		parent::init();
	}

	public function CreateAuctionLink() {
		return TmCreateAuctionPage::get()->first()->Link();
	}

	public function DeleteAuctionLink($id) {
		return Controller::join_links($this->Link(), 'deleteAuction', '?id=' . $id);
	}

	public function deleteAuction() {
		$id = $this->getRequest()->getVar('id');
		$auction = TmAuction::get()->filter('ID', $id)->first();
		if ($auction) {
			$auction->deleteAuction();
		}
	}

	public function SetSessionMessage($message) {
		Session::set('AuctionSessionMessage', $message);
	}

	public function SessionMessage() {
		$message = Session::get('AuctionSessionMessage');
		Session::clear('AuctionSessionMessage');
		return $message;
	}
}