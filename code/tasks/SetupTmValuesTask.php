<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 11/05/17
 * Time: 8:39 AM
 */
class SetupTmValuesTask extends BuildTask {

	/**
	 * Implement this method in the task subclass to
	 * execute via the TaskRunner
	 */
	public function run($request) {
		$duration = array(
		  2 => 'Two',
		  3 => 'Three',
		  4 => 'Four',
		  5 => 'Five',
		  6 => 'Six',
		  7 => 'Seven',
		  10 => 'Ten',
		  14 => 'Fourteen',
		  21 => 'TwentyOne',
		  28 => 'TwentyEight',
		);

		foreach ($duration as $val => $name) {
			$duration = TmDuration::get()->filter('Value', $val)->first();
			if (!$duration) {
				$duration = TmDuration::create();
				$duration->Title = $name;
				$duration->Value = $val;
				$duration->write();
			}
		}

//------------------
		$pickup = array(
		  'None' => 0,
		  'Allow' => 1,
		  'Demand' => 2,
		  'Forbid' => 3,
		);

		foreach ($pickup as $name => $val) {
			$pickup = TmPickup::get()->filter('Value', $val)->first();
			if (!$pickup) {
				$pickup = TmPickup::create();
				$pickup->Title = $name;
				$pickup->Value = $val;
				$pickup->write();
			}
		}

//------------------
		$shipping = array(
		  'Courier Fixed Small' => 5,
		  'Courier Fixed Medium' => 10,
		  'Courier Fixed Large' => 15,
		  'Free' => 0,
		);

		foreach ($shipping as $name => $val) {
			$option = TmShippingOption::get()->filter('Price', $val)->first();
			if (!$option) {
				$option = TmShippingOption::create();
				$option->Method = $name;
				$option->Price = $val;
				$option->Type = ($val == 0 ? 'Free' : 'Custom');
				$option->Value = ($val == 0 ? 3 : 4);
				$option->write();
			}
		}

		//------------------
		$paymentMethod = array(
		  'BankDeposit' => 1,
		  'CreditCard' => 2,
		  'Cash' => 4,
		  'SafeTrader' => 8,
		);

		foreach ($paymentMethod as $name => $val) {
			$option = TmPaymentMethod::get()->filter('Value', $val)->first();
			if (!$option) {
				$option = TmPaymentMethod::create();
				$option->Title = $name;
				$option->Value = $val;
				$option->write();
			}
		}
	}
}