<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 19/07/16
 * Time: 4:05 PM
 */
class TmCategoryFillTask extends BuildTask {

	/**
	 * Implement this method in the task subclass to
	 * execute via the TaskRunner
	 */
	public function run($request) {
		// TODO: Implement run() method.
		$tm = TmApi::create();
		$categories = $tm->Category()->getCategories();
		TmCategory::FillFromApi($categories['Subcategories']);
	}
}