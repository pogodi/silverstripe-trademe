<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 19/07/16
 * Time: 10:59 AM
 */
class TmSiteConfigExtension extends DataExtension {

	private static $db = array(
	  'TmConsumerKey' => 'Varchar(255)',
	  'TmConsumerSecret' => 'Varchar(255)',
	  'TmOAuthToken' => 'Varchar(255)',
	  'TmOAuthTokenSecret' => 'Varchar(255)'
	);

	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldToTab('Root.Trademe', TextField::create('TmConsumerKey', 'Trademe Consumer Key')->setDescription('get it from My Trademe / My Trade Me API applications'));
		$fields->addFieldToTab('Root.Trademe', TextField::create('TmConsumerSecret', 'Trademe Consumer Secret'));

		$fields->addFieldToTab('Root.Trademe', TextField::create('TmOAuthToken', 'Trademe OAuth Token')->setDescription('generate it at http://developer.trademe.co.nz/api-overview/authentication/'));
		$fields->addFieldToTab('Root.Trademe', TextField::create('TmOAuthTokenSecret', 'Trademe OAuth Token Secret'));
	}
}