<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 19/07/16
 * Time: 12:13 PM
 */
class TmApiAuction extends TmBase {

	protected $url = 'Selling';
	protected $RequestType = 'POST';

	protected $deleteUrl = 'Withdraw';

	/**
	 * push auction to Trademe
	 *
	 * @param $tmAuction
	 */
	public function createListing($tmAuction) {
		$json = $tmAuction->toJson();
		$debug = fopen(BASE_PATH . '/request.txt', 'w');
		$this->getRequest(
		  [
		    'debug' => $this->debug(),
			'json' => $json
		  ]
		);
		$test = $this->Request->getBody()->__toString();
		$response = $this->Client->send($this->Request);
		return $response->json();
	}

	public function deleteListing($tmAuction) {
		$json = array(
		  'ListingId' => (int) $tmAuction->ForeignID,
		  'ReturnListingDetails' => true,
		  'SalePrice' => $tmAuction->BuyNowPrice,
		  'Type' => 1,
		);
		$this->getRequest(
		  [
			'debug' => $this->debug(),
			'json' => $json
		  ],
		  null,
		  self::$Endpoint . $this->url . '/' . $this->deleteUrl . $this->format
		);
		$test = $this->Request->getBody()->__toString();
		$response = $this->Client->send($this->Request);

		return $response->json();
	}

	protected function debug() {
		return $debug = fopen(BASE_PATH . '/request.txt', 'w');
	}
}