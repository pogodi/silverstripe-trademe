<?php
use GuzzleHttp\Client;
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 19/07/16
 * Time: 1:07 PM
 */
class TmBase extends Object {
	protected static $Endpoint;
	protected $RequestType;
	protected $format = '.json';
	protected $url;

	protected $Client;
	/**
	 * @var \GuzzleHttp\Message\Request
	 */
	protected $Request;

	public function authenticate() {
		$config = Config::inst();

		if (empty($config->get('TmBase','Environment'))) {
			die ('Please set what environment you want to use with Trademe (sandbox or live)');
		} else {
			if ($config->get('TmBase', 'Environment') == 'live') {
				self::$Endpoint = $config->get('TmBase', 'Endpoint');
			} else {
				self::$Endpoint = $config->get('TmBase', 'EndpointDev');
			}
		}

		return $this;
	}

	public function getRequest($options = array(), $headers = null, $urlOverwrite = null) {
		$this->Client = new Client();

		if (!$urlOverwrite) {
			$url = self::$Endpoint . $this->url . $this->format;
		} else {
			$url = $urlOverwrite;
		}

		$this->Request = $this->Client->createRequest($this->RequestType, $url, $options);
		$this->Request->setHeader('Authorization', implode(' ', array(
		  'OAuth',
		  'oauth_consumer_key=' . SiteConfig::current_site_config()->TmConsumerKey . ',',
		  'oauth_token=' . SiteConfig::current_site_config()->TmOAuthToken . ',',
		  'oauth_version=' . '1.0' . ',',
		  'oauth_timestamp=' . time() . ',',
		  'oauth_nonce=' . uniqid() . ',',
		  'oauth_signature_method=' . 'PLAINTEXT' . ',',
		  'oauth_signature=' . SiteConfig::current_site_config()->TmConsumerSecret . urlencode('&') . SiteConfig::current_site_config()->TmOAuthTokenSecret,
		)));

		if ($headers) {
			foreach ($headers as $name => $val) {
				$this->Request->addHeader(
				  $name, $val
				);
			}
		}
	}
}