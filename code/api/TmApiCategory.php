<?php
use GuzzleHttp\Client;
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 19/07/16
 * Time: 12:14 PM
 */
class TmApiCategory extends TmBase {

	protected $url = 'Categories';
	protected $RequestType = 'GET';

	public function getCategories() {
		$this->getRequest();
		$response = $this->Client->send($this->Request);
		return $response->json();
	}
}