<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 19/07/16
 * Time: 10:37 AM
 */
class TmApi extends TmBase {

	public static $xmlNS = 'http://api.trademe.co.nz/v1';

	/**
	 * @return TmApiAuction
	 */
	public function Listing() {
		return TmApiAuction::create()->authenticate();
	}

	/**
	 * @return TmApiCategory
	 */
	public function Category() {
		return TmApiCategory::create()->authenticate();
	}

	/**
	 * @return TmApiImage
	 */
	public function TmApiImage() {
		return TmApiImage::create()->authenticate();
	}
}