<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 19/07/16
 * Time: 5:18 PM
 */
class TmPaymentMethod extends DataObject {
	private static $db = array(
	  'Title' => 'Varchar(255)',
	  'Value' => 'Int'
	);

	private static $belongs_many_many = array(
	  'Auctions' => 'TmAuction'
	);

	public static function toJson($items) {
		$return = [];
		foreach ($items as $item) {
			$return[] = $item->Value;
		}
		return $return;
	}
}