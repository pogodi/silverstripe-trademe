<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 19/07/16
 * Time: 5:12 PM
 */
class TmShippingOption extends DataObject {
	private static $db = array(
	  'Price' => 'Currency',
	  'Method' => 'Varchar(50)',
	  'Type' => "Enum('Pickup,Free,Custom','Custom')",
	  'Value' => 'Int'
	);

	private static $belongs_many_many = array(
	  'Auctions' => 'TmAuction'
	);

	public static function toJson($items) {
		$return = [];
		foreach ($items as $item) {
			if ($item->Type == 'Custom') {
				$return[] = (object) [
				  'Type' => $item->Value,
				  'Price' => $item->Price,
				  'Method' => $item->Method
				];
			} else {
				$return[] = (object) [
				  'Type' => $item->Value
				];
			}
		}
		return $return;
	}
}