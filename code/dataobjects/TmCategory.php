<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 19/07/16
 * Time: 10:51 AM
 */
class TmCategory extends DataObject{
	private static $db = array(
	  'Name' => 'Varchar(255)',
	  'Path' => 'Varchar(255)',
	  'HasClassifieds' => 'Boolean',
	  'ForeignID' => 'Varchar(255)',
	  'IsLeaf' => 'Boolean',
	  'BranchNameCache' => 'Varchar(255)'
	);

	private static $has_one = array(
	  'Parent' => 'TmCategory'
	);

	private static $has_many = array(
	  'Children' => 'TmCategory'
	);

	public static function FillFromApi($result, $parent = 0) {
		foreach ($result as $item) {
			$obj = TmCategory::create();
			$obj->Name = $item['Name'];
			$obj->Path = $item['Path'];
			$obj->ForeignID = $item['Number'];
			$obj->HasClassifieds = isset($item['HasClassifieds']) && $item['HasClassifieds'] == 'yes' ? true : false;
			$obj->ParentID = $parent;
			$obj->write();
			if (isset($item['Subcategories']) && count($item['Subcategories'])) {
				self::FillFromApi($item['Subcategories'], $obj->ID);
			}
		}
	}

	public static function LeafCategories() {
		$return = array();
		if (TmCategory::get()->filter('IsLeaf', true)->first()) {
			$categories = TmCategory::get()->filter('IsLeaf', true);
		} else {
			$categories = TmCategory::get();
		}

		foreach ($categories as $category) {
			if ($category->IsLeaf) {
				if ($category->BranchNameCache) {
					$return[$category->ID] = $category->BranchNameCache;
				} else {
					$return[$category->ID] = $category->BranchName();
					$category->BranchNameCache = $return[$category->ID];
					$category->write();
				}

			}
			elseif (!$category->Children()->Count()) {
				$return[$category->ID] = $category->BranchName();
				$category->IsLeaf = true;
				$category->write();
			}
		}

		return $return;
	}

	public function BranchName($divider = ' | ') {
		$tmpArray = array();
		$page = $this;
		while ($page->ID) {
			$tmpArray[] = $page->Name;
			$page = $page->Parent();
		}
		return implode(' ' . $divider .' ', array_reverse($tmpArray));
	}
}