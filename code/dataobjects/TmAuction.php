<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 19/07/16
 * Time: 10:38 AM
 */
class TmAuction extends DataObject {
	private static $db = array(
	  'Title' => 'Varchar(50)',
	  'SubTitle' => 'Varchar(50)',
	  'Description' => 'Text',
	  'StartPrice' => 'Currency',
	  'ReservePrice' => 'Currency',
	  'BuyNowPrice' => 'Currency',
	  'EndDateTime' => 'SS_DateTime', // only if duration is a specific date (value: 0),
	  'IsBrandNew' => 'Boolean',
	  'AuthenticatedMembersOnly' => 'Boolean',
	  'IsClassified' => 'Boolean', // category dependend
	  'IsBold' => 'Boolean',
	  'IsFeatured' => 'Boolean',
	  'IsHomepageFeatured' => 'Boolean',
	  'HasGallery' => 'Boolean',
	  'HasGalleryPlus' => 'Boolean',
	  'Quantity' => 'Int', // Setting the quantity (even to 1) will prevent bidding and make a listing only purchasable via Buy Now.
	  'IsFlatShippingCharge' => 'Boolean', // buy now only, when more than one are bought
	  'IsHighlighted' => 'Boolean',
	  'HasSuperFeature' => 'Boolean',
	  'ExternalReferenceId' => 'Varchar(50)',
	  'SKU' => 'Varchar(50)',
	  'ForeignID' => 'Varchar(255)'
	);

	private static $has_one = array(
	  'Category' => 'TmCategory',
	  'Duration' => 'TmDuration',
	  'Pickup' => 'TmPickup',
	  'Product' => 'Product'
	);

	private static $has_many = array(
	  'Photos' => 'TmImage'
	);

	private static $many_many = array(
	  'ShippingOptions' => 'TmShippingOption',
	  'PaymentMethods' => 'TmPaymentMethod'
	);

	private static $exclude_json = array(
	  'ForeignID'
	);

	public function toJson() {
		$properties = array_diff(self::$db, self::$exclude_json);

		$jsonPrep = [];
		foreach ($properties as $property => $type) {
			if ($property == 'Description') {
				$jsonPrep[$property] = explode("\n", $this->$property);
			}
			elseif(self::$db[$property] == 'Boolean') {
				$jsonPrep[$property] = ($this->$property == 1) ? true : false;
			}
			elseif ($this->$property) {
				$jsonPrep[$property] = $this->$property;
			}
		}

		$categoryList = array_filter(explode('-', $this->Category()->ForeignID));
		$jsonPrep["Category"] = (int) end($categoryList);
		$jsonPrep["Duration"] = $this->Duration()->Value;
		$jsonPrep["Pickup"] = $this->Pickup()->Value;

		$jsonPrep["ShippingOptions"] = TmShippingOption::toJson($this->ShippingOptions());
		$jsonPrep["PaymentMethods"] = TmPaymentMethod::toJson($this->PaymentMethods());

		return $jsonPrep;
	}

	public static function running() {
		$test = TmAuction::get()->filter(array(
		  'ForeignID:GreaterThan' => 0
		))->toArray();


		return TmAuction::get()->filter(array(
		  'ForeignID:GreaterThan' => 0
		));
	}

	public function deleteAuction() {
		$auction = TmApi::create()->Listing();

		try {
			$result = $auction->deleteListing($this);
			if (is_array($result)) {
				if (isset($result['Success']) && $result['Success'] == false) {
					Controller::curr()->SetSessionMessage($result['Description']);
					return Controller::curr()->redirectBack();
				} elseif(isset($result['Success']) && $result['Success'] == true) {
					$this->delete();
					Controller::curr()->SetSessionMessage('Auction removed');
					return Controller::curr()->redirectBack();
				}
			}
		} catch (Exception $E) {
			Controller::curr()->SetSessionMessage('Problem removing Auction: ' . $E->getMessage());
		}

	}

	public function Ends() {
		if ($this->EndDate) return $this->EndDate;

		$started = $this->Created;
		$startedTimestamp = strtotime($started);
		$days = $this->Duration()->Value;
		$startedTimestamp += $days * (3600*24);

		$return = SS_Datetime::create();
		$return->setValue($startedTimestamp);
		return $return;
	}
}